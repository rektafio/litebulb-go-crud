package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"os"
)

func loadJSON() []UserDetails {

	var users map[string][]UserDetails

	bytes, err := ioutil.ReadFile("data.json")
	if err != nil {
		fmt.Print(err)
	}

	err = json.Unmarshal(bytes, &users)
	if err != nil {
		log.Fatalf("Unable to decode the json")
	}

	var userSlice []UserDetails
	userSlice = users["users"]

	return userSlice
}

func saveJSON() {

	m := make(map[string][]UserDetails)
	m["users"] = userDetailsSlice

	bytes, err := json.Marshal(m)
	if err != nil {
		fmt.Print(err)
	}

	err = os.Remove("data.json")
	if err != nil {
		fmt.Print(err)
	}

	err = ioutil.WriteFile("data.json", bytes, 0)
	if err != nil {
		fmt.Print(err)
	}
}

func createUser(user UserDetails) {
	userDetailsSlice = append(userDetailsSlice, user)
	saveJSON()
}

func deleteUser(userName string) {

	var newSlice []UserDetails

	for i, n := range userDetailsSlice {
		if userDetailsSlice[i].Name != userName {
			newSlice = append(newSlice, n)
		}
	}
	userDetailsSlice = newSlice
	saveJSON()
}

func updateUser(userName string, updatedUser UserDetails) {

	for i := 0; i < len(userDetailsSlice); i++ {
		if userDetailsSlice[i].Name == userName {
			userDetailsSlice[i] = updatedUser
		}
	}
	saveJSON()
}

/* func insertUser(user UserDetails) {

	b, err := json.Marshal(user)
	if err != nil {
		fmt.Print(err)
	}

	ioutil.WriteFile("data/"+user.Name+".json", b, 0)
	fmt.Println("The users details are:", string(b))
} */

/* func getUser(name string) {

	var user UserDetails

	b, err := ioutil.ReadFile(name + ".json")
	if err != nil {
		fmt.Print(err)
	}

	err = json.Unmarshal(b, &user)
	if err != nil {
		log.Fatalf("Unable to decode the json")
	}
} */

/* func getAllUsers() []UserDetails {

	var user UserDetails
	var users []UserDetails

	files, err := ioutil.ReadDir("data")
	if err != nil {
		log.Fatal(err)
	}

	for _, file := range files {

		data, err := ioutil.ReadFile("data/" + file.Name())
		if err != nil {
			fmt.Print(err)
		}

		err = json.Unmarshal(data, &user)
		if err != nil {
			log.Fatalf("Unable to decode the json")
		}

		users = append(users, user)
	}
	return users
} */
