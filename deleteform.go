package main

import (
	"fmt"
	"net/http"
)

func deleteformHandler(w http.ResponseWriter, r *http.Request) {

	if r.Method == http.MethodPost {
		name := r.FormValue("name")

		deleteUser(name)

		err := t.ExecuteTemplate(w, "datatable", userDetailsSlice)
		if err != nil {
			fmt.Println(http.StatusInternalServerError)
		}
	}

	if r.Method != http.MethodPost {
		err := t.ExecuteTemplate(w, "deleteform", nil)
		if err != nil {
			fmt.Println(http.StatusInternalServerError)
		}
	}

}
