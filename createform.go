package main

import (
	"fmt"
	"net/http"
)

func createformHandler(w http.ResponseWriter, r *http.Request) {

	if r.Method == http.MethodPost {
		user := UserDetails{
			Name:  r.FormValue("name"),
			Age:   r.FormValue("age"),
			Email: r.FormValue("email"),
		}

		createUser(user)

		err := t.ExecuteTemplate(w, "datatable", userDetailsSlice)
		if err != nil {
			fmt.Println(http.StatusInternalServerError)
		}
	}

	if r.Method != http.MethodPost {
		err := t.ExecuteTemplate(w, "createform", nil)
		if err != nil {
			fmt.Println(http.StatusInternalServerError)
		}
	}

}
