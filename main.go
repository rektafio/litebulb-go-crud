package main

import (
	"fmt"
	"html/template"
	"net/http"
	"os"
)

// UserDetails - User details
type UserDetails struct {
	Name  string `json:"name"`
	Age   string `json:"age"`
	Email string `json:"email"`
}

var t *template.Template

var userDetailsSlice []UserDetails

func main() {

	var err error
	userDetailsSlice = loadJSON()

	port := os.Getenv("PORT")
	if port == "" {
		port = "3000"
	}

	mux := http.NewServeMux()
	fs := http.FileServer(http.Dir("assets"))

	t, err = template.ParseFiles(
		"templates/index.html",
		"templates/navigation.html",
		"templates/datatable.html",
		"templates/createform.html",
		"templates/deleteform.html",
		"templates/updateform.html",
	)
	if err != nil {
		panic(err)
	}

	mux.Handle("/assets/", http.StripPrefix("/assets/", fs))

	mux.HandleFunc("/", indexHandler)
	mux.HandleFunc("/datatable", datatableHandler)
	mux.HandleFunc("/createform", createformHandler)
	mux.HandleFunc("/deleteform", deleteformHandler)
	mux.HandleFunc("/updateform", updateformHandler)

	fmt.Println("Server is running on port:", port)
	http.ListenAndServe(":"+port, mux)
}
